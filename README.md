# Links

* [Designing projects for `FetchContent`](https://www.foonathan.net/2022/06/cmake-fetchcontent/)
* ["build an external library"](https://stackoverflow.com/questions/63667653/how-to-build-an-external-library-downloaded-with-cmake-fetchcontent)
* [cmake usefulness of aliases](https://stackoverflow.com/questions/46567646/cmake-usefulness-of-aliases)

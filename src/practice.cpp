#include "practice_lib/practice.h"

#include <iostream>

namespace eymonttdev {

void execute()
{
    std::cout << "Hello world!" << std::endl;
}

} // namespace eymonttdev